import string


class Analysis:
    """
    Class for common operations in weather & football problem
    """
    def analyze(self):
        """
        Performs common operation of weather and football problem
        """
        if not self.index_col_1 is None and not self.index_col_2 is None and not self.index_col_3 is None:
            difference = float('inf')
            for row in self.data:
                if row.strip().startswith(tuple(string.digits)):
                    fields = row.split()
                    identifier = fields[self.index_col_1]
                    minuend = int(fields[self.index_col_2].strip(string.punctuation))
                    subtrahend = int(fields[self.index_col_3].strip(string.punctuation))
                    gap = abs(minuend - subtrahend)
                    if gap < difference:
                        difference = gap
                        result = identifier
            return f'Identifier: {result} with Difference: {difference}'
        else:
            return 'Cannot Analyze : Incomplete Input'
