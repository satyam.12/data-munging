from data_analysis import Analysis
from data_extraction import DataExtraction
from configparser import ConfigParser

config = ConfigParser()
config.read('config.ini')

class FootballAnalysis(Analysis):
    """
    Class for football problem that inherits Analyse,  class & uses inherited methods
    to compute result
    """
    def __init__(self,filename,index_col_1=1,index_col_2=6,index_col_3=8):
        self.data = DataExtraction.data_extraction(self,filename)
        self.index_col_1 = index_col_1
        self.index_col_2 = index_col_2
        self.index_col_3 = index_col_3


football = FootballAnalysis(config['data']['football_path'])
print(football.analyze())