from data_analysis import Analysis
from data_extraction import DataExtraction
from configparser import ConfigParser

config = ConfigParser()
config.read('config.ini')

class WeatherAnalysis(Analysis):
    """
    Class for weather problem that inherits Analyse class & uses inherited methods
    to compute result
    """
    def __init__(self,filename,index_col_1=0,index_col_2=1,index_col_3=2):
        self.data = DataExtraction.data_extraction(self,filename)
        self.index_col_1 = index_col_1
        self.index_col_2 = index_col_2
        self.index_col_3 = index_col_3

weather = WeatherAnalysis(config['data']['weather_path'])
print(weather.analyze())